import Plataforma from './Plataforma';
import Jogo from './Jogo';
import Utilizador from './Utilizador';

class Comentarios {
  constructor(texto, plataforma, jogo, avaliacao, utilizador) {
    if( plataforma instanceof Plataforma &&
        jogo instanceof Jogo &&
        utilizador instanceof Utilizador) {
      this.texto = texto;
      this.plataforma = plataforma;
      this.jogo = jogo;
      this.avaliacao = avaliacao;
      this.utilizador = utilizador;
    } else throw TypeError;
  }
}