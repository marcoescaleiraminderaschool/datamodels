class Avaliacao {
  constructor(rating, comentarios) {
    this.rating = rating;
    this.comentarios = comentarios;
  }
}

export default Avaliacao;