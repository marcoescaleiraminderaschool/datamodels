import Jogo from './Jogo';

class Plataforma {
  constructor(nome, jogos = []) {
    if (jogos.length > 0 && jogos instanceof Jogo) {
      this.nome = nome;
      this.jogos = jogos;
    } else throw TypeError;
  }
}

export default Plataforma;