import Genero from './Genero';
import Plataforma from './Plataforma';

class Jogo {
  constructor(titulo, generos = [], plataforma = []) {
    if ((generos.length > 0 && generos instanceof Genero) && (plataforma.length > 0 && plataforma instanceof Plataforma)) {
      this.titulo = titulo
      this.generos = generos
      this.plataformas = plataforma
    } else throw TypeError;
  }
}

export default Jogo;