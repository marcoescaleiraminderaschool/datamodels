class Genero {
  constructor(tipo, jogos = []) {
    this.tipo = tipo
    this.jogos = jogos
  }
}

export default Genero;